# 测试1 格式： `![](2.jpg)  或![](22.png)`  桌面软件正常  
![](2.jpg)


# 测试2 格式： `![](22 -(2).png)`  

![](22 -(2).png)  

# 测试3 格式： `![](22_3$.png)`   桌面软件正常  
 
![](22_3$.png)


# 测试4 格式： `![](22-副本.png)`  
![](22-副本.png)


# 测试5 格式： `![](test!.jpg)`  
 test!.jpg 、test#.jpg 、test%.jpg 、test.444.png、test@.jpg桌面软件正常   
test^.jpg 、test·.png 桌面软件不显示  

test@.jpg gitlab显示
![](test!.jpg "test!.jpg") 
--- 
test%.jpg   

![](test%.jpg)  
---
test^.jpg  

![](test^.jpg)
---
# 测试6 格式： `![](test·.png)` 
![](test·.jpg)

# 测试7 格式： `![](test-6.jpg)`  桌面软件正常 
![](test-6.jpg)

# 测试8 格式： `![](测试.jpg)`  
![](测试.jpg)


# 测试9 格式： `![](test .jpg)`  有空格 不显示
![](test .jpg)



# 测试10 格式： `![](test$.jpg)`   桌面软件正常  
 
![](test$.jpg)

# 测试11 格式： `![](test！.jpg)`   中文感叹号  
 
![](test！.jpg)


# 测试12 格式： `![](test.444.png)`   英文句号在中间  
![](test.444.png)




# 测试13 格式： `![](../../test/img/1.jpg)`    
 
![](../../test/img/1.jpg "../../test/img/1.jpg")

# 测试14 格式： `![](../test/img/1.jpg)`     
 
![](../test/img/1.jpg "../test/img/1.jpg")
