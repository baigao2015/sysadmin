
# 编辑Markdown文档 插入图片（相对路径）相关要求及注意事项

分享类型：markdown文档插入图片规范  
分享人：CK    邮箱：`chenkui@wondersgroup.com`  
分享时间： 2016-03-04   
修订历史  
|日期 | 版本 | 作者 | 修改内容 |  
|---|:---|:---|:---|   
|  2016-03-04   | 0.1 |  CK  | 初次提交分享 |      

## 1.概要
为了规范markdown图片路径问题，为了更好地兼容github、gitlab里wiki模块以及其他编辑markdown文档的软件的图片显示问题。在此对用户编辑Markdown文档时使用相对路径插入图片提出相应的要求。

## 2.插入图片主要格式 
  **`![](图片路径 "图片名称")`**  
-- | ----- |  ----------  |   
--可填--必填 -----可填  
如： `![xxxx](xxxx.png "log")`   
对应的HTML代码是：  
`<img alt="xxxx" src="xxxx.png" title="log">`  

<font color=green>**强烈建议: 全部填写。 `![图片不能显示时此处填写时则显示相应内容](图片路径 "title提示内容")`**</font>

## 3.命名规范  
主要以gitlab上的wiki模块来规范。
  
   1). 图片名称<font color=red >禁用</font>以下方式：  
    ① 中文、 空格   
    ②  ~  \ / " ? < > | ^  ·(中文的·)      
    ③ 其他待完善 


   2). 文件夹或路径或目录均<font color=red >不能含以下几种情况：</font>  
    ① 中文  空格   
    ② ·   ~  \ / " ? < > | ^  
    ③ 其他待完善 

 

## 4.使用方法：  

    |--project //项目名称
       |--test.png
       |--test.md
    

比如把test.png的图片和md文件放在一起，那么就可以用这种方式插入图片：`![](test.png)` 或 `![test](test.png)` 或`![test](test.png "test")`  

    |--project //项目名称
       |--images
          |---test.png
       |--test.md

如果不想放在同一层级,那么就可以这样插入:`![](images/test.png)` 或 `![test](images/test.png)` 或`![test](images/test.png "test")` 表示引用同层级一个叫做"img"的文件夹中的test.png图片，应该所有markdown软件都支持这种写法的。（<font color="green" >提倡此方法</font>） 
   

    |--project //项目名称
       |--images
          |---test.png
       |--uploads
          |--test.md


如果图片放在上一层级文件夹里,那么就可以这样插入:`![](../images/test.png)` 或 `![test](../images/test.png)` 或`![test](../images/test.png "test")` 表示引用上一层层级一个叫做"img"的文件夹中的test.png图片。<font color=red> *（这种写法gitlab以及github里是支持的，但`MarkdownPad 2`软件测试过不支持，不知道其他软件是否也不支持，待完善。）* </font> <font color="green" >（不提倡选此方法）</font>  
   

<font color=red >**注意：不管在同级或同层都不应该再添加"`./`"或"/"(主要原因是在gitlab里不能很好地处理，导致图片不能正常显示)。**  </font>  

<font color=green>**强烈建议: 文件夹以及文件名最好以英文字母或数字来命名，不应用运算符、标点符号来命名，禁用特殊符号命名。**</font>


## 规范小结  

| 操作 | 实例 | 备注 |  状态 |    
|---|:---|:---|:---:|  
| 文件夹或图片名称命名含：中英文及数字 | test图片1.png、test图片.png 、图片1.png  | 图片不能显示 ，不支持此写法 | <font color=red>✘</font> |  
| 文件夹或图片名称命名含：中文 | 图片1.png  | 图片不能显示 ，不支持此写法 | <font color=red>✘</font> |  
| 文件夹或图片名称命名含：特殊字符 | ㈱㊣★☆♀.png  | 图片不能显示 ，不支持此写法 | <font color=red>✘</font> |      
| 文件夹或图片名称命名含：含有~!/@#$%^&*()-_=+\[{}];:'\",<.>/? 符号其一或多个 | test@!~1.png 、test@.png |有的能显示，有的不能显示，建议不要含`~!/@#$%^&*()\[{}];:\/'\",<.>/?`,但可以使用`-_`。 | <font color=red>✘</font><font color=green>✔</font>  |   
| md文档及图片在同一层级时加`./`或`/` | `![test](./test.jpg "test")` | 图片不能显示 ，github/gitlab里wiki模块不支持此写法 | <font color=red>✘</font> |  
| md文档及图片在上一层级时加`../` | `![test](../test.jpg "test")` 或`![test](../images/test.jpg "test")` | github/gitlab里正常显示，但在`MarkdownPad 2`软件打开时不显示 | <font color=green>✔</font> <font color=red>✘</font> |  

  
  

